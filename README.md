# KANJIHOLIC, only for kanji addicts. 

Android App to group, practice and master your favorite Kanjis.

Features: 

* Firebase Authentication for multiple users.
* Cloud storage with Firestore.
* Local storage to use it offline.
* Group your kanjis into decks.
* Practice with random order. 

Developed with Typescript in React-Native. 100% functional components. 

Note: This App is integrated to a web Administration Panel available in kanjiholic.com. (still in development)
