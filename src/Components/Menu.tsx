import React, {FC, useState} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {Menu, MenuItem, MenuDivider} from 'react-native-material-menu';
import Icon from 'react-native-vector-icons/Octicons';
import confirmDialog from './confirmDialog';
import {signOut} from '../Storage/firebase';
import {COLORS} from '../Utils/constants';
import {TouchableOpacity} from 'react-native-gesture-handler';

const HamburguerMenu: FC = () => {
  const [visible, setVisible] = useState(false);

  const hideMenu = () => setVisible(false);

  const showMenu = () => setVisible(true);
  const handleSignOut = () => {
    confirmDialog(signOut, 'Do you really want to logout?');
    hideMenu();
  };
  const LogOutButton = () => {
    return (
      <Text style={{color: COLORS.orange}} onPress={handleSignOut}>
        LOGOUT
      </Text>
    );
  };
  const MenuIcon = () => (
    <TouchableOpacity style={styles.hamburguer} onPress={showMenu}>
      <Icon name="three-bars" size={30} color="#000" />
    </TouchableOpacity>
  );

  return (
    <View style={styles.main}>
      <Menu
        style={styles.menu}
        visible={visible}
        anchor={<MenuIcon />}
        onRequestClose={hideMenu}>
        <MenuItem style={styles.menuItem} onPress={hideMenu}>
          <LogOutButton />
        </MenuItem>
      </Menu>
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'white',
  },
  menu: {
    backgroundColor: COLORS.blue,
  },
  item: {
    color: 'white',
  },
  menuItem: {
    alignItems: 'center',
  },
  hamburguer: {
    color: 'white',
    right: 10,
  },
});
export default HamburguerMenu;
